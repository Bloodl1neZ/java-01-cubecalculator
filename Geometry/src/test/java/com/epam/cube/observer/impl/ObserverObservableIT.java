package com.epam.cube.observer.impl;

import com.epam.cube.entity.Point3D;
import com.epam.cube.service.CubeParameters;
import org.junit.Assert;
import org.junit.Test;


public class ObserverObservableIT {
    private static final double DELTA = 0.01;

    private final CubeObserver cubeObserver = new CubeObserver();

    @Test
    public void shouldAddObserverAndSetSideAndRecalculateParameters () {
        Point3D point3D = new Point3D(1, 1, 1);
        double sideLength = 3;
        CubeObservable cubeObservable = new CubeObservable(point3D, sideLength, 1);
        cubeObserver.addObservable(cubeObservable);
        CubeParameters parametersBefore = cubeObserver.getCubeParametersById(cubeObservable.getId());

        cubeObservable.setSideLength(sideLength + 1);
        CubeParameters parametersAfter = cubeObserver.getCubeParametersById(cubeObservable.getId());
        Assert.assertNotEquals(parametersBefore, parametersAfter);

        Assert.assertEquals(64, parametersAfter.getVolume(), DELTA);

        Assert.assertEquals(96, parametersAfter.getSurfaceArea(), DELTA);
    }

    @Test
    public void shouldGetCubeParamsOfUnexistingIdAndReturnNull() {
        //when
        CubeParameters cubeParameters = cubeObserver.getCubeParametersById(3);
        //then
        Assert.assertEquals(null, cubeParameters);
    }

    @Test
    public void shouldAddAndRemoveAndGetNullParameters() {
        Point3D point3D = new Point3D(1, 1, 1);
        double sideLength = 3;
        long id = 1;
        CubeObservable cubeObservable = new CubeObservable(point3D, sideLength, id);
        cubeObserver.addObservable(cubeObservable);
        cubeObserver.removeObservable(cubeObservable);

        CubeParameters cubeParameters = cubeObserver.getCubeParametersById(id);

        Assert.assertEquals(null, cubeParameters);
    }
}
