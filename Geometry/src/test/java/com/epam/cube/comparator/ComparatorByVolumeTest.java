package com.epam.cube.comparator;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class ComparatorByVolumeTest {
    private static final Point3D POINT_3_D = new Point3D(2, 2, 2);
    private static final double FIRST_SIDE_LENGTH = 4;
    private static final double SECOND_SIDE_LENGTH = 5;
    private static final Cube FIRST_CUBE = new Cube(POINT_3_D, FIRST_SIDE_LENGTH, 1);
    private static final Cube SECOND_CUBE = new Cube(POINT_3_D, SECOND_SIDE_LENGTH, 2);

    private final Comparator<Cube> comparator = new ComparatorByVolume();

    @Test
    public void shouldCompareAndReturnNegativeNumber() {
        //when
        int result = comparator.compare(FIRST_CUBE, SECOND_CUBE);

        Assert.assertTrue(result < 0);
    }

    @Test
    public void shouldCompareAndReturnPositiveNumber() {
        //when
        int result = comparator.compare(SECOND_CUBE, FIRST_CUBE);

        Assert.assertTrue(result > 0);
    }

    @Test
    public void shouldCompareAndReturnZero() {
        //when
        int result = comparator.compare(SECOND_CUBE, SECOND_CUBE);

        Assert.assertTrue(result == 0);
    }
}
