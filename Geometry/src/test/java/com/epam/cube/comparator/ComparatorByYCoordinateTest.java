package com.epam.cube.comparator;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import org.junit.Assert;
import org.junit.Test;

import java.util.Comparator;

public class ComparatorByYCoordinateTest {
    private static final Point3D FIRST_SPOINT_3_D = new Point3D(2, 1, 2);
    private static final Point3D SECOND_POINT_3_D = new Point3D(2, 2, 2);
    private static final double SIDE_LENGTH = 4;
    private static final Cube FIRST_CUBE = new Cube(FIRST_SPOINT_3_D, SIDE_LENGTH, 1);
    private static final Cube SECOND_CUBE = new Cube(SECOND_POINT_3_D, SIDE_LENGTH, 2);

    private final Comparator<Cube> comparator = new ComparatorByYCoordinate();

    @Test
    public void shouldCompareAndReturnNegativeNumber() {
        //when
        int result = comparator.compare(FIRST_CUBE, SECOND_CUBE);
        //then
        Assert.assertTrue(result < 0);
    }

    @Test
    public void shouldCompareAndReturnPositiveNumber() {
        //when
        int result = comparator.compare(SECOND_CUBE, FIRST_CUBE);
        //then
        Assert.assertTrue(result > 0);
    }

    @Test
    public void shouldCompareAndReturnZero() {
        //when
        int result = comparator.compare(SECOND_CUBE, SECOND_CUBE);
        //then
        Assert.assertTrue(result == 0);
    }
}
