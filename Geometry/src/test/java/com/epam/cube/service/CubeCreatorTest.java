package com.epam.cube.service;

import com.epam.cube.entity.Cube;
import com.epam.cube.validator.CubeValidator;
import com.epam.cube.validator.impl.CubeValidatorImpl;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.List;

public class CubeCreatorTest {

    @Test
    public void shouldCreateListOfCubesWithLengthThree() {
        //given
        CubeValidator mockCubeValidator = Mockito.mock(CubeValidatorImpl.class);
        Mockito.when(mockCubeValidator.isValid(Mockito.any(double[].class))).thenReturn(true);
        CubeCreator cubeCreator = new CubeCreator(mockCubeValidator);
        double firstArr[] = {1, 2, 3, 4};
        double secondArr[] = {1, 3, 3, 4};
        double thirdArr[] = {4, 2, 3, 6};
        List<double[]> doubles = new ArrayList<>();
        doubles.add(firstArr);
        doubles.add(secondArr);
        doubles.add(thirdArr);
        //when
        List<Cube> cubeList = cubeCreator.createCubes(doubles);
        //then
        Assert.assertEquals(3, cubeList.size());

    }

    @Test
    public void shouldCreateEmptyListOfCubes() {
        //given
        CubeValidator mockCubeValidator = Mockito.mock(CubeValidatorImpl.class);
        Mockito.when(mockCubeValidator.isValid(Mockito.any(double[].class))).thenReturn(false);
        CubeCreator cubeCreator = new CubeCreator(mockCubeValidator);
        double firstArr[] = null;
        List<double[]> doubles = new ArrayList<>();
        doubles.add(firstArr);
        //when
        List<Cube> cubeList = cubeCreator.createCubes(doubles);
        //then
        Assert.assertTrue(cubeList.isEmpty());
    }
}
