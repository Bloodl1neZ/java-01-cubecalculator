package com.epam.cube.service;

import org.junit.Assert;
import org.junit.Test;

public class IdGeneratorTest {
    private final IdGenerator idGenerator = IdGenerator.getInstance();

    @Test
    public void shouldReturnIdEqualsOneAndThenIdEqualsTwo() {
        long firstResult = idGenerator.calculateNextCubeId();

        Assert.assertEquals(1, firstResult);

        long secondResult = idGenerator.calculateNextCubeId();

        Assert.assertEquals(2, secondResult);
    }
}
