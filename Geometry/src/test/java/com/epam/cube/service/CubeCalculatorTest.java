package com.epam.cube.service;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;

public class CubeCalculatorTest {
    private static final Point3D POINT_3_D = new Point3D(1.5, 0,0.5);
    private static final double LENGTH = 3;
    private static final long ID = 1;
    private static final Cube CUBE = new Cube(POINT_3_D, LENGTH, ID);
    private static final double DELTA = 0.01;

    private final CubeCalculator cubeCalculator = new CubeCalculator();

    @Test
    public void shouldCalculateAndReturnVolumeEqualsTwentySevenWhenLengthThree() {
        //when
        double result = cubeCalculator.calculateVolume(CUBE);
        //then
        Assert.assertEquals(27, result, DELTA);
    }

    @Test
    public void shouldCalculateAndReturnSurfaceAreaEqualsFiftyFourWhenLengthTwo() {
        //when
        double result = cubeCalculator.calculateSurfaceArea(CUBE);
        //then
        Assert.assertEquals(54, result, DELTA);
    }

    @Test
    public void shouldCalculateAndReturnRatioOfVolumesDevidedByXOYEqualsZeroPointFive() {
        //when
        double ratio = cubeCalculator.calculateRatioOfVolumesDevidedByPlane(CUBE, Plane.XOY);
        //then
        Assert.assertEquals(0.5, ratio, DELTA);
    }

    @Test
    public void shouldCalculateAndReturnRatioOfVolumesDevidedByXOZEqualsOne() {
        //when
        double ratio = cubeCalculator.calculateRatioOfVolumesDevidedByPlane(CUBE, Plane.XOZ);
        //then
        Assert.assertEquals(1, ratio, DELTA);
    }

    @Test
    public void shouldCalculateAndReturnRatioOfVolumesDevidedByYOZEqualsZero() {
        //when
        double ratio = cubeCalculator.calculateRatioOfVolumesDevidedByPlane(CUBE, Plane.YOZ);
        //then
        Assert.assertEquals(0, ratio, DELTA);
    }

    @Test
    public void shouldCalculatePlanesAndWithCubeLocatesAndReturnListWithYOZ() {
        //when
        List<Plane> planeList = cubeCalculator.calculateListOfPlanesOnWitchCubeLocates(CUBE);
        //then
        Assert.assertEquals(1, planeList.size());

        Assert.assertEquals(Plane.YOZ, planeList.get(0));
    }

}
