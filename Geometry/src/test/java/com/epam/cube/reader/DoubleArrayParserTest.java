package com.epam.cube.reader;

import org.junit.Assert;
import org.junit.Test;

public class DoubleArrayParserTest {
    private final DoubleArrayParser doubleArrayParser = new DoubleArrayParser();

    @Test
    public void shouldParseAndReturnArray() {
        //given
        String string = "    -2.00000010    +23.4 -7553.343 +334  ";
        //when
        double [] arr = doubleArrayParser.parseFourDoublesFromString(string);
        //then
        Assert.assertEquals(4, arr.length);
    }

    @Test
    public void shouldNotParseAndReturnNull() {
        //given
        String string = "    -2.000000000    +23.4f -7553.343 +334  ";

        //when
        double [] arr = doubleArrayParser.parseFourDoublesFromString(string);

        //then
        Assert.assertEquals(null, arr);
    }
}

