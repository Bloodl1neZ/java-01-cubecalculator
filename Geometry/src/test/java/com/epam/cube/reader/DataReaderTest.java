package com.epam.cube.reader;

import com.epam.cube.exception.InvalidPathException;
import org.junit.Assert;
import org.junit.Test;
import java.util.List;

public class DataReaderTest {
    private final DataReader dataReader = new DataReader();

    @Test
    public void shouldReadAndReturnListOfStringWithLengthFour() throws InvalidPathException {
        //given
        String path = "src/test/resources/file.txt";
        //when
        List<String> list = dataReader.getStringsFromFile(path);
        //then
        Assert.assertEquals(8, list.size());

    }

    @Test(expected = InvalidPathException.class)
    public void shouldThrowExpcetionWrongDir() throws InvalidPathException{
        //given
        String path = "qwe.txt";
        //when
        dataReader.getStringsFromFile(path);
    }


}
