package com.epam.cube.validator.impl;

import com.epam.cube.validator.CubeValidator;
import org.junit.Assert;
import org.junit.Test;

public class CubeValidatorImplTest {
    private final CubeValidator cubeValidatorImpl = new CubeValidatorImpl();

    @Test
    public void shouldCheckA0rrayAndReturnFalse() {
        //given
        double[] arr = {1, 2, -3, -4};
        //when
        boolean result = cubeValidatorImpl.isValid(arr);
        //then
        Assert.assertFalse(result);

    }

    @Test
    public void shouldCheckArrayAndReturnTrue() {
        //given
        double[] arr = {1, 2, -3, 3};
        //when
        boolean result = cubeValidatorImpl.isValid(arr);
        //then
        Assert.assertTrue(result);

    }

}
