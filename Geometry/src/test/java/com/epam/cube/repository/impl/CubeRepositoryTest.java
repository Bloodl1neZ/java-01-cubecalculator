package com.epam.cube.repository.impl;

import com.epam.cube.comparator.ComparatorById;
import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.repository.Repository;
import com.epam.cube.repository.Specification;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.Comparator;
import java.util.List;

public class CubeRepositoryTest {
    private static final Point3D POINT_3_D = new Point3D(1, 1, 25);
    private static final Cube FIRST_CUBE = new Cube(POINT_3_D, 4, 1);
    private static final Cube SECOND_CUBE = new Cube(POINT_3_D, 5, 3);
    private static final Cube THIRD_CUBE = new Cube(POINT_3_D, 6, 2);
    private static final Cube FOURTH_CUBE = new Cube(POINT_3_D, 7, 4);

    private final Repository<Cube> repository = new CubeRepository();

    {
        repository.add(FIRST_CUBE);
        repository.add(SECOND_CUBE);
        repository.add(THIRD_CUBE);
        repository.add(FOURTH_CUBE);
    }

    @Test
    public void shouldFind() {
        Specification<Cube> mockSpec = Mockito.mock(Specification.class);
        Mockito.when(mockSpec.specify(Mockito.any(Cube.class))).thenReturn(true);
        List<Cube> result = repository.find(mockSpec);

        Assert.assertEquals(4, result.size());

        Cube firstCube = result.get(0);

        Assert.assertEquals(FIRST_CUBE, firstCube);
    }

    @Test
    public void shouldSortByIdAndReturnList() {
        //given
        Comparator<Cube> mockComp = Mockito.mock(ComparatorById.class);
        Mockito.when(mockComp.compare(Mockito.any(Cube.class), Mockito.any(Cube.class))).thenReturn(1);
        //when
        List<Cube> list = repository.sort(mockComp);
        //then
        Assert.assertNotEquals(null, list);
        System.out.println(list);
        Cube firstCube = list.get(0);

        Assert.assertEquals(1, firstCube.getId());
    }
}
