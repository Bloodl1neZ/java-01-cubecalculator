package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.repository.Specification;
import org.junit.Assert;
import org.junit.Test;

public class IdSameSpecificationTest {
    private static final long SPEC_ID = 2;
    private static final Point3D POINT_3_D = new Point3D(1, 1, 25);
    private static final Cube FIRST_CUBE = new Cube(POINT_3_D, 4, 1);
    private static final Cube SECOND_CUBE = new Cube(POINT_3_D, 6, 2);
    private static final Cube THIRD_CUBE = new Cube(POINT_3_D, 6, 3);

    private final Specification<Cube> specification = new IdSameSpecification(SPEC_ID);

    @Test
    public void shouldCheckCubeWithIdTwoAndReturnTrue() {
        //when
        boolean result = specification.specify(SECOND_CUBE);
        //then
        Assert.assertTrue(result);
    }

    @Test
    public void shouldCheckCubeWithIdOneAndReturnFalse() {
        //when
        boolean result = specification.specify(FIRST_CUBE);
        //then
        Assert.assertFalse(result);
    }

    @Test
    public void shouldCheckCubeWithThreeOneAndReturnFalse() {
        //when
        boolean result = specification.specify(THIRD_CUBE);
        //then
        Assert.assertFalse(result);
    }
}
