package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.repository.Specification;
import org.junit.Assert;
import org.junit.Test;

public class DistanceBetweenCenterOfCubeAndCoordAxesLowerThenSpecificationTest {
    private static final double DISTANCE = 13.5;
    private static final Point3D FIRST_POINT_3_D = new Point3D(-3, -4, 12);
    private static final Point3D SECOND_POINT_3_D = new Point3D(3, -4, 13);
    private static final Cube FIRST_CUBE = new Cube(FIRST_POINT_3_D, 3, 1);
    private static final Cube SECOND_CUBE = new Cube(SECOND_POINT_3_D, 2.2, 2);

    private final Specification<Cube> spec = new DistanceBetweenCenterOfCubeAndCoordAxesLowerThenSpecification(DISTANCE);

    @Test
    public void shouldCheckCubeWithZEqualsTwelveAndReturnTrue() {
        //when
        boolean result = spec.specify(FIRST_CUBE);
        //then
        Assert.assertTrue(result);
    }

    @Test
    public void shouldCheckCubeWithZEqualsThirteenAndReturnFalse() {
        //when
        boolean result = spec.specify(SECOND_CUBE);
        //then
        Assert.assertFalse(result);
    }

}
