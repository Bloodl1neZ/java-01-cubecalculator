package com.epam.cube.repository.impl;

import com.epam.cube.comparator.ComparatorById;
import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.repository.Repository;
import com.epam.cube.repository.impl.specification.IdSameSpecification;
import org.junit.Assert;
import org.junit.Test;
import java.util.Comparator;
import java.util.List;

public class CubeRepositoryIT {
    private static final Double DELTA = 0.01;
    private static final Point3D POINT_3_D = new Point3D(1, 1, 25);
    private static final Cube FIRST_CUBE = new Cube(POINT_3_D, 4, 1);
    private static final Cube SECOND_CUBE = new Cube(POINT_3_D, 5, 3);
    private static final Cube THIRD_CUBE = new Cube(POINT_3_D, 6, 2);
    private static final Cube FOURTH_CUBE = new Cube(POINT_3_D, 7, 4);
    private static final Cube FIFTH_CUBE = new Cube(POINT_3_D, 7, 1);

    private final Repository<Cube> repository = new CubeRepository();

    {
        repository.add(FIRST_CUBE);
        repository.add(SECOND_CUBE);
        repository.add(THIRD_CUBE);
        repository.add(FOURTH_CUBE);
    }

    @Test
    public void shouldUpdateAndReturnListWithNewCube() {
        repository.update(FIFTH_CUBE);
        long fifthCubeId = FIFTH_CUBE.getId();
        IdSameSpecification idSpec = new IdSameSpecification(fifthCubeId);
        List<Cube> list = repository.find(idSpec);
        Cube newCube = list.get(0);

        Assert.assertEquals(FIFTH_CUBE, newCube);
    }

    @Test
    public void shouldSortByIdAndReturnList() {
        //given
        Comparator<Cube> comp = new ComparatorById();
        //when
        List<Cube> list = repository.sort(comp);
        //then
        Assert.assertNotEquals(null, list);

        Cube firstCube = list.get(0);

        Assert.assertEquals(1, firstCube.getId());
    }

    @Test
    public void shouldRemoveAndReturnEmptyList() {
        repository.remove(FIRST_CUBE);

        long firstCubeId = FIRST_CUBE.getId();
        IdSameSpecification idSpec = new IdSameSpecification(firstCubeId);

        List<Cube> list = repository.find(idSpec);

        Assert.assertEquals(0, list.size());
    }
}
