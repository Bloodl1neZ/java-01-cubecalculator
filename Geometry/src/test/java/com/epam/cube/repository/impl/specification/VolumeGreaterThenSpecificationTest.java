package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.repository.Specification;
import org.junit.Assert;
import org.junit.Test;

public class VolumeGreaterThenSpecificationTest {
    private static final Point3D POINT_3_D = new Point3D(1, 1, 25);
    private static final Cube FIRST_CUBE = new Cube(POINT_3_D, 1, 1);
    private static final Cube SECOND_CUBE = new Cube(POINT_3_D, 4, 3);
    private static final int VOLUME = 8;

    private final Specification<Cube> specification = new VolumeGreaterThenSpecification(VOLUME);

    @Test
    public void shouldCheckCubeWithIdThreeAndReturnTrue() {
        //when
        boolean result = specification.specify(SECOND_CUBE);
        //then
        Assert.assertTrue(result);
    }

    @Test
    public void shouldCheckCubeWithIdOneAndReturnFalse() {
        //when
        boolean result = specification.specify(FIRST_CUBE);
        //then
        Assert.assertFalse(result);
    }
}
