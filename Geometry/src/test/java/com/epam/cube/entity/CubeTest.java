package com.epam.cube.entity;

import org.junit.Assert;
import org.junit.Test;

public class CubeTest {
    private static final double DELTA = 0.01;

    @Test
    public void shouldReturnSideLength() {
        //given
        double coord = 2;
        Point3D point3D = new Point3D(coord, coord, coord);
        double sideLength = 4;
        long id = 1;
        Cube cube = new Cube(point3D, sideLength, id);

        //when
        double result = cube.getSideLength();

        //then
        Assert.assertEquals(4, result, DELTA);
    }


}
