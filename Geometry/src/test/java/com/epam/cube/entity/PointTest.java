package com.epam.cube.entity;

import org.junit.Assert;
import org.junit.Test;

public class PointTest {
    private static final double DELTA = 0.01;

    @Test
    public void shouldCreatePointWithXFive() {
        //given
        double x = 5;
        double y = 0;
        double z = 2;

        Point3D point3D = new Point3D(x, y ,z);

        //when
        double result = point3D.getX();

        //then
        Assert.assertEquals(5, result, DELTA);


    }
}
