package com.epam.cube.validator;

public interface CubeValidator{
    boolean isValid(double[] arr);
}
