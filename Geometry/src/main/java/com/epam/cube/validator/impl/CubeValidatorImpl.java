package com.epam.cube.validator.impl;

import com.epam.cube.validator.CubeValidator;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

import java.util.Arrays;

public class CubeValidatorImpl implements CubeValidator {
    private static final Logger LOGGER = LogManager.getLogger(CubeValidatorImpl.class);

    private final int arraySize = 4;

    public boolean isValid(final double[] arr) {
        boolean isValid = ((arr != null)
                && (arr.length == arraySize)
                && (arr[arraySize - 1] > 0));
        if(isValid) {
            LOGGER.info("Array " + Arrays.toString(arr) + " is valid to create cube");
        } else {
            LOGGER.warn("Array " + Arrays.toString(arr) + " is not valid to create cube");
        }
        return isValid;
    }
}
