package com.epam.cube.comparator;

import com.epam.cube.entity.Cube;
import com.epam.cube.service.CubeCalculator;

import java.util.Comparator;

public class ComparatorBySurfaceArea implements Comparator<Cube> {
    private final CubeCalculator cubeCalculator = new CubeCalculator();

    @Override
    public int compare(Cube firstCube, Cube secondCube) {
        double firstCubeSurfaceArea = cubeCalculator.calculateSurfaceArea(firstCube);
        double secondCubeSurfaceArea = cubeCalculator.calculateSurfaceArea(secondCube);
        return Double.compare(firstCubeSurfaceArea, secondCubeSurfaceArea);
    }
}