package com.epam.cube.comparator;
import com.epam.cube.entity.Cube;

import java.util.Comparator;

public class ComparatorById implements Comparator<Cube> {
    @Override
    public int compare(Cube firstCube, Cube secondCube) {
        long firstCubeId = firstCube.getId();
        long secondCubeId = secondCube.getId();
        return Long.compare(firstCubeId, secondCubeId);
    }
}
