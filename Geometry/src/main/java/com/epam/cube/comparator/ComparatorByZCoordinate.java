package com.epam.cube.comparator;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;

import java.util.Comparator;

public class ComparatorByZCoordinate implements Comparator<Cube> {
    @Override
    public int compare(Cube firstCube, Cube secondCube) {
        Point3D firstCubePoint3D = firstCube.getPoint();
        Point3D secondCubePoint3D = secondCube.getPoint();
        double firstCubeZ = firstCubePoint3D.getZ();
        double secondCubeZ = secondCubePoint3D.getZ();
        return Double.compare(firstCubeZ, secondCubeZ);
    }
}
