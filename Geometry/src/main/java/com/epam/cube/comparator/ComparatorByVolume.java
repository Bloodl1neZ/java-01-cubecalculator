package com.epam.cube.comparator;
import com.epam.cube.entity.Cube;
import com.epam.cube.service.CubeCalculator;

import java.util.Comparator;

public class ComparatorByVolume implements Comparator<Cube> {
    private final CubeCalculator cubeCalculator = new CubeCalculator();

    @Override
    public int compare(Cube firstCube, Cube secondCube) {
        double firstCubeVolube = cubeCalculator.calculateVolume(firstCube);
        double secondCubeVolube = cubeCalculator.calculateVolume(secondCube);
        return Double.compare(firstCubeVolube, secondCubeVolube);
    }
}
