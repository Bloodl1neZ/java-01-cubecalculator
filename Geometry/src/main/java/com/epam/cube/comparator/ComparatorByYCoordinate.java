package com.epam.cube.comparator;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;

import java.util.Comparator;

public class ComparatorByYCoordinate implements Comparator<Cube> {
    @Override
    public int compare(Cube firstCube, Cube secondCube) {
        Point3D firstCubePoint3D = firstCube.getPoint();
        Point3D secondCubePoint3D = secondCube.getPoint();
        double firstCubeY = firstCubePoint3D.getY();
        double secondCubeY = secondCubePoint3D.getY();
        return Double.compare(firstCubeY, secondCubeY);
    }
}

