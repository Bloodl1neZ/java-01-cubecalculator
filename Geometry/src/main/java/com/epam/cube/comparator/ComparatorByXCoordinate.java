package com.epam.cube.comparator;
import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;

import java.util.Comparator;

public class ComparatorByXCoordinate implements Comparator<Cube> {
    @Override
    public int compare(Cube firstCube, Cube secondCube) {
        Point3D firstCubePoint3D = firstCube.getPoint();
        Point3D secondCubePoint3D = secondCube.getPoint();
        double firstCubeX = firstCubePoint3D.getX();
        double secondCubeX = secondCubePoint3D.getX();
        return Double.compare(firstCubeX, secondCubeX);
    }
}
