package com.epam.cube.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Cube {
    private static final Logger LOGGER = LogManager.getLogger(Cube.class);

    private final long id;
    private Point3D point;
    private double sideLength;

    public Cube(Point3D point, double sideLength, long id) {
        this.id = id;
        this.point = point;
        this.sideLength = sideLength;

        LOGGER.info("Сube has been created");
    }

    public Point3D getPoint() {
        return point;
    }

    public double getSideLength() {
        return sideLength;
    }

    public long getId() {
        return id;
    }

    public void setPoint(Point3D point) {
        this.point = point;
    }

    public void setSideLength(double sideLength) {
        this.sideLength = sideLength;
    }

    @Override
    public String toString() {
        String str = "Cube. id =" + id + ". " + point + ". Side length = " + sideLength;
        return str;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Cube cube = (Cube) object;
        return (id == cube.id
                && sideLength == cube.sideLength
                && point.equals(cube.point));
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + ((point == null) ? 0 : point.hashCode());
        result = prime * result + Double.hashCode(sideLength);
        result = prime * result + Long.hashCode(id);
        return result;
    }
}
