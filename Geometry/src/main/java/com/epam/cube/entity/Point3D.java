package com.epam.cube.entity;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Point3D {
    private static final Logger LOGGER = LogManager.getLogger(Point3D.class);

    private final double x;
    private final double y;
    private final double z;

    public Point3D(double x, double y, double z) {
        this.x = x;
        this.y = y;
        this.z = z;
        LOGGER.debug("Point3D has been created");
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    @Override
    public String toString() {
        String str = "Point3D. x = " + x + ", y = " + y + ", z = " + z;
        return str;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        Point3D point3D = (Point3D) object;
        return (x == point3D.x
                && y == point3D.y
                && z == point3D.z);
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + Double.hashCode(x);
        result = prime * result + Double.hashCode(y);
        result = prime * result + Double.hashCode(z);
        return result;
    }

}
