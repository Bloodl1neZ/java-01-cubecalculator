package com.epam.cube.service;

public class IdGenerator {
    private static IdGenerator instance;

    private long nextCubeId = 1;

    private IdGenerator() {

    }

    public long calculateNextCubeId() {
        return nextCubeId++;
    }

    public static IdGenerator getInstance() {
        if(instance == null) {
            instance = new IdGenerator();
        }
        return instance;
    }
}
