package com.epam.cube.service;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.validator.CubeValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.util.List;
import java.util.ArrayList;

public class CubeCreator {
    private static final Logger LOGGER = LogManager.getLogger(CubeCreator.class);

    private final CubeValidator cubeValidator;

    public CubeCreator(CubeValidator cubeValidator) {
        this.cubeValidator = cubeValidator;
        LOGGER.info("CubeCreator has been created");
    }

    public List<Cube> createCubes(final List<double[]> listOfArrays) {
        List<Cube> list = new ArrayList<>();
        for (double[] it : listOfArrays) {
            if(cubeValidator.isValid(it)) {
                list.add(createCubeViaArray(it));
            }
        }
        LOGGER.info("List of Cubes has been created");
        return list;
    }

    private Cube createCubeViaArray(final double[] arr) {
        Point3D point3D = new Point3D(arr[0], arr[1], arr[2]);
        double sideLength = arr[3];
        IdGenerator idGenerator = IdGenerator.getInstance();
        return new Cube(point3D, sideLength, idGenerator.calculateNextCubeId());
    }
}
