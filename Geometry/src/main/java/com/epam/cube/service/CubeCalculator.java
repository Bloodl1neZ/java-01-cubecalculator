package com.epam.cube.service;

import com.epam.cube.entity.*;
import com.epam.cube.exception.NotSupportedPlaneException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;


public class CubeCalculator {
    private static final Logger LOGGER = LogManager.getLogger(CubeCalculator.class);

    public CubeCalculator() {
        LOGGER.info("CubeCalculator has been created");
    }

    public double calculateVolume(final Cube cube) {
        double sideLength = cube.getSideLength();
        double volume = Math.pow(sideLength, 3);
        LOGGER.info("Volume of cube has been calculated");
        return volume;
    }

    public double calculateSurfaceArea(final Cube cube) {
        double sideLength = cube.getSideLength();
        double surfaceArea = Math.pow(sideLength, 2) * 6;
        LOGGER.info("Surface area of cube has been calculated");
        return surfaceArea;
    }

    public double calculateRatioOfVolumesDevidedByPlane(final Cube cube, final Plane plane) {
        double volume;
        Point3D point3D = cube.getPoint();
        double halfOfSideLength = cube.getSideLength() / 2;
        switch (plane) {
        case XOY:
            volume = calculateRatioOfVolumesByCoordinate(halfOfSideLength, point3D.getZ());
            break;
        case YOZ:
            volume = calculateRatioOfVolumesByCoordinate(halfOfSideLength, point3D.getX());
            break;
        case XOZ:
            volume = calculateRatioOfVolumesByCoordinate(halfOfSideLength, point3D.getY());
            break;
        default:
            throw new NotSupportedPlaneException(plane);
        }
        LOGGER.info("Ratio of volumes has been calculated");
        return volume;
    }

    private double calculateRatioOfVolumesByCoordinate(double halfOfSideLength, double coordOfPoint) {
        double absCoordOfPoint = Math.abs(coordOfPoint);
        double ratioOne = halfOfSideLength - absCoordOfPoint;
        double ratioTwo = halfOfSideLength + absCoordOfPoint;
        if(absCoordOfPoint >= halfOfSideLength) {
            ratioOne = 0;
        }
        return ratioOne / ratioTwo;
    }

    public List<Plane> calculateListOfPlanesOnWitchCubeLocates(final Cube cube) {
        List<Plane> list = new ArrayList<>();
        Point3D point = cube.getPoint();
        double halfOfSideLength = cube.getSideLength() / 2;
        if(halfOfSideLength == Math.abs(point.getZ())) {
            list.add(Plane.XOY);
        }
        if(halfOfSideLength == Math.abs(point.getX())) {
            list.add(Plane.YOZ);
        }
        if(halfOfSideLength == Math.abs(point.getY())) {
            list.add(Plane.XOZ);
        }
        LOGGER.info("List of planes has been calculated");
        if(list.isEmpty()) {
            LOGGER.debug("There are no planes on witch cube locates");
        }
        return list;
    }
}