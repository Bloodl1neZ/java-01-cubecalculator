package com.epam.cube.service;

public class CubeParameters {
    private final double volume;
    private final double surfaceArea;

    public CubeParameters(double volume, double surfaceArea) {
        this.volume = volume;
        this.surfaceArea = surfaceArea;
    }

    public double getVolume() {
        return volume;
    }

    public double getSurfaceArea() {
        return surfaceArea;
    }

    @Override
    public String toString() {
        String str = "CubeParameters. Volume =" + volume + ". Surface area = " + surfaceArea;
        return str;
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) {
            return true;
        }
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        CubeParameters cubeParameters = (CubeParameters) object;
        return (volume == cubeParameters.volume
                && surfaceArea == cubeParameters.surfaceArea);
    }

    @Override
    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = prime * result + Double.hashCode(surfaceArea);
        result = prime * result + Double.hashCode(volume);
        return result;
    }
}
