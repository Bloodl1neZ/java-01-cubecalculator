package com.epam.cube.observer.impl;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.service.CubeCalculator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class CubeObservable extends Cube {
    private static final Logger LOGGER = LogManager.getLogger(CubeCalculator.class);

    private final List<CubeObserver> observerList;

    public CubeObservable(final Point3D point3D, double sideLength, long id) {
        super(point3D, sideLength, id);
        observerList = new ArrayList<>();
    }

    @Override
    public void setSideLength(double sideLength) {
        super.setSideLength(sideLength);
        notifyObservers();
    }

    public void addObserver(CubeObserver cubeObserver) {
        observerList.add(cubeObserver);
    }

    public void removeObserver(CubeObserver cubeObserver) {
        observerList.remove(cubeObserver);
    }

    private void notifyObservers() {
        for(CubeObserver observer : observerList) {
            observer.handleEvent(this);
        }
        LOGGER.info("Observers have been notified. Parameters have been changed.");
    }
}
