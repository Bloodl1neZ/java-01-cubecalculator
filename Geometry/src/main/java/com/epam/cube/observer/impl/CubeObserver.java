package com.epam.cube.observer.impl;

import com.epam.cube.entity.Cube;
import com.epam.cube.observer.Observer;
import com.epam.cube.service.CubeCalculator;
import com.epam.cube.service.CubeParameters;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class CubeObserver implements Observer {
    private static final Logger LOGGER = LogManager.getLogger(CubeCalculator.class);

    private final Map<Long, CubeParameters> data = new HashMap<>();
    private final CubeCalculator CUBE_CALCULATOR = new CubeCalculator();

    public void handleEvent(Cube cube) {
        CubeParameters cubeParameters = calculateParameters(cube);
        data.replace(cube.getId(), cubeParameters);
    }

    public void addObservable(CubeObservable observableCube) {
        observableCube.addObserver(this);
        CubeParameters cubeParameters = calculateParameters(observableCube);
        data.put(observableCube.getId(), cubeParameters);
        LOGGER.info("Observable cube has been added.");
    }

    public void removeObservable(CubeObservable observableCube) {
        observableCube.removeObserver(this);
        data.remove(observableCube.getId());
        LOGGER.info("Observable cube has been removed.");
    }

    private CubeParameters calculateParameters(Cube cube) {
        double volume = CUBE_CALCULATOR.calculateVolume(cube);
        double surfaceArea = CUBE_CALCULATOR.calculateSurfaceArea(cube);
        CubeParameters cubeParameters = new CubeParameters(volume, surfaceArea);
        return cubeParameters;
    }

    public CubeParameters getCubeParametersById(long id) {
        return data.get(id);
    }
}
