package com.epam.cube.observer;

import com.epam.cube.entity.Cube;

public interface Observer {
    void handleEvent(Cube cube);
}
