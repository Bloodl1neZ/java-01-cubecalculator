package com.epam.cube.exception;

import java.io.IOException;

public class InvalidPathException extends IOException {
    public InvalidPathException(String path, IOException ex) {
        super(path, ex);
    }
}
