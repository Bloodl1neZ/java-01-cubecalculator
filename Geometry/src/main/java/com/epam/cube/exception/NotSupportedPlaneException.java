package com.epam.cube.exception;

import com.epam.cube.service.Plane;

public class NotSupportedPlaneException extends RuntimeException {
    public NotSupportedPlaneException(Plane plane) {
        super("Not supported plane: " + plane.toString());
    }
}

