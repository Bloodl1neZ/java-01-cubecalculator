package com.epam.cube.repository;

import java.util.Comparator;
import java.util.List;

public interface Repository<T> {
    void add(T object);
    void update(T object);
    void remove(T object);
    List<T> find(Specification specification);
    List<T> sort(Comparator<T> comparator);
}
