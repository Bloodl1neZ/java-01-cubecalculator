package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.repository.Specification;
import com.epam.cube.service.CubeCalculator;

public class VolumeGreaterThenSpecification implements Specification<Cube> {
    private final double volume;
    private final CubeCalculator cubeCalculator;

    public VolumeGreaterThenSpecification(double volume) {
        this.volume = volume;
        cubeCalculator = new CubeCalculator();
    }

    @Override
    public boolean specify(Cube object) {
        double currentVolume = cubeCalculator.calculateVolume(object);
        return (Double.compare(currentVolume, volume) > 0);
    }
}
