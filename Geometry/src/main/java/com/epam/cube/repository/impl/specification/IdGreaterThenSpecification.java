package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.repository.Specification;

public class IdGreaterThenSpecification implements Specification<Cube> {
    private final long id;

    public IdGreaterThenSpecification(long id) {
        this.id = id;
    }

    @Override
    public boolean specify(Cube object) {
        long currentId = object.getId();
        return (currentId > id);
    }
}
