package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.repository.Specification;
import com.epam.cube.service.CubeCalculator;

public class SurfaceAreaGreaterThenSpecification implements Specification<Cube> {
    private final double surfaceArea;
    private final CubeCalculator cubeCalculator;

    public SurfaceAreaGreaterThenSpecification(double surfaceArea) {
        this.surfaceArea = surfaceArea;
        cubeCalculator = new CubeCalculator();
    }

    @Override
    public boolean specify(Cube object) {
        double currentSurfaceArea = cubeCalculator.calculateSurfaceArea(object);
        return (Double.compare(currentSurfaceArea, surfaceArea) > 0);
    }
}
