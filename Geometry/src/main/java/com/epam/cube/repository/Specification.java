package com.epam.cube.repository;

public interface Specification<T> {
    boolean specify(T object);
}
