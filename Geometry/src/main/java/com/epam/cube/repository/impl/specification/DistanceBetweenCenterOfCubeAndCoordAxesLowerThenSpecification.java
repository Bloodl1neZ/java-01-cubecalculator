package com.epam.cube.repository.impl.specification;

import com.epam.cube.entity.Cube;
import com.epam.cube.entity.Point3D;
import com.epam.cube.repository.Specification;

public class DistanceBetweenCenterOfCubeAndCoordAxesLowerThenSpecification implements Specification<Cube> {
    private final double distance;

    public DistanceBetweenCenterOfCubeAndCoordAxesLowerThenSpecification(double distance) {
        this.distance = distance;
    }

    @Override
    public boolean specify(Cube object) {
        Point3D point3D = object.getPoint();
        double currentDistance = calculateDistanceByPoint(point3D);
        return (Double.compare(currentDistance, distance) < 0);
    }

    private double calculateDistanceByPoint(Point3D point3D) {
        double x = point3D.getX();
        double y = point3D.getY();
        double z = point3D.getZ();
        double xy = Math.hypot(x, y);
        double xyz = Math.hypot(xy, z);
        return xyz;
    }
}
