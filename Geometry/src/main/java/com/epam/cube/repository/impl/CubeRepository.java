package com.epam.cube.repository.impl;
import com.epam.cube.entity.Cube;
import com.epam.cube.reader.DataReader;
import com.epam.cube.repository.Repository;
import com.epam.cube.repository.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

public class CubeRepository implements Repository<Cube> {
    private static final Logger LOGGER = LogManager.getLogger(DataReader.class);

    private final Map<Long, Cube> data = new HashMap<>();

    public void add(Cube cube) {
        long incomeCubeId = cube.getId();
        data.put(incomeCubeId, cube);
    }

    @Override
    public List<Cube> find(Specification specification) {
        return data.values().stream().
                filter(o -> specification.specify(o))
                .collect(Collectors.toList());
    }

    @Override
    public void update(Cube cube) {
        long incomeCubeId = cube.getId();
        data.replace(incomeCubeId, cube);
    }

    @Override
    public void remove(Cube cube) {
        long incomeCubeId = cube.getId();
        data.remove(incomeCubeId);
    }

    @Override
    public List<Cube> sort(Comparator<Cube> comparator) {
        List<Cube> list = new ArrayList<>(data.values());
        list.sort(comparator);
        LOGGER.info("List has been created and sorted.");
        return list;
    }
}
