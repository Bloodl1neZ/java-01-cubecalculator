package com.epam.cube.reader;

import com.epam.cube.exception.InvalidPathException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class DataReader {
    private static final Logger LOGGER = LogManager.getLogger(DataReader.class);

    public DataReader() {
        LOGGER.info("DataReader has been created");
    }

    public List<String> getStringsFromFile(final String path) throws InvalidPathException {
        List<String> list = new ArrayList<>();

        try (BufferedReader br = new BufferedReader(new FileReader(path))) {
            LOGGER.debug("Path is correct");
            addStringsToList(br, list);
        } catch (IOException ex) {
            LOGGER.info("IOException catched", ex);
            throw new InvalidPathException(path, ex);
        }
        return list;
    }

    private void addStringsToList(final BufferedReader br, final List<String> list) throws IOException {
        String line = null;
        while((line = br.readLine()) != null ) {
            list.add(line);
        }
        LOGGER.info("Strings have been added to the list");
    }
}
