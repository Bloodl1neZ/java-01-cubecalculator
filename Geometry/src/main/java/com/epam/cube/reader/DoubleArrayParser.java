package com.epam.cube.reader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DoubleArrayParser {
    private static final Logger LOGGER = LogManager.getLogger(DoubleArrayParser.class);

    private final String regDoubleNumber = "\\+?-?[0-9]+\\.?[0-9]*";
    private final String regOneOrMoreSpaces = "\\s+";
    private final String regZeroOrMoreSpaces = "\\s*";
    private final int arraySize = 4;

    public DoubleArrayParser() {
        LOGGER.info("DoubleArrayParser has been created");
    }

    public double [] parseFourDoublesFromString(final String string) {
        double[] arr = null;
        if(isMatchesToFourDoubles(string)) {
            LOGGER.debug("String is valid");
            arr = new double[arraySize];
            Pattern pattern = Pattern.compile(regDoubleNumber);
            Matcher matcher = pattern.matcher(string);

            for (int i = 0; i < arr.length; i++) {
                matcher.find();
                Double d = Double.parseDouble(matcher.group());
                arr[i] = d;
            }
            LOGGER.info("Double array parsing has been finished successfuly");
        }
        if(arr == null) {
            LOGGER.warn("Double array parsing has been failed (not valid string)");
        }
        return arr;
    }

    private boolean isMatchesToFourDoubles(final String string) {
        final String regThreeDoubleNumbers = regZeroOrMoreSpaces + regDoubleNumber
                + regOneOrMoreSpaces + regDoubleNumber + regOneOrMoreSpaces
                + regDoubleNumber + regOneOrMoreSpaces
                + regDoubleNumber + regZeroOrMoreSpaces;
        Pattern pattern = Pattern.compile(regThreeDoubleNumbers);
        Matcher matcherOfWholeString = pattern.matcher(string);
        if(matcherOfWholeString.matches()) {
            LOGGER.info("String [" + string + "] is matches to array of 4 doubles");
        } else {
            LOGGER.info("String [" + string + "] is not matches to array of 4 doubles");
        }
        return matcherOfWholeString.matches();
    }

}
